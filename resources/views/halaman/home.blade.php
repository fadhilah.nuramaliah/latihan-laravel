<!DOCTYPE html>
@extends('layout.master')
@section('judul')
    Sanberbook
@endsection
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
@section('content')
<body>

        <div class="top-wrapper">
            <div class="container">
                <div class="konten1">
                    <h2>Social Media Developer Santai Berkualitas</h2>
                    <p>Belajar dan Berbagi agar hidup ini semakin santai berkualitas</p>

                </div>

                <div class="konten2">
                    <h3>Benefit Join di SanberBook</h3>
                    <ul>
                        <li>Mendapatkan motivasi dari sesama developer</li>
                        <li>Sharing knowledge dari para mastah Sanber</li>
                        <li>Dibuat oleh calon web developer terbaik</li>
                    </ul>

                </div>

                <div class="konten3">
                    <h3>Cara Bergabung ke SanberBook</h3>
                    <ol>
                        <li>Mengunjungi Website ini</li>
                        <li>Mendaftar di <a href="/register">Form Sign Up</a></li>
                        <li>Selesai!</li>
                    </ol>
                </div>
            </div>




        </div>
</body>
@endsection
</html>
